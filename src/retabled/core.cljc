(ns retabled.core
  (:require [retabled.internal :as internal]
            #?(:cljs [reagent.core :refer [atom]])
            #?(:cljs [retabled.jscore :as jscore])))

(def col-map-help
  "Possible values of col-maps within `:columns` of control-map "
  [{:valfn (fn [entry] "Retrieves the value of this cell from `entry`")
    :sortfn (fn [entry] "Given an entry, how to sort it by this field. Defaults to `valfn`.")
    :displayfn (fn [valfn-value] "Produces the display from result `(valfn entry)`. 
                                  Default `identity`")
    :headline "The string to display in table header"
    :css-class-fn (fn [entry] "Produces class (str or vector) to be applied to field/column")
    :ignore-case? "Whether to ignore the case during filtering"
    :filter "If truthy displays a filter-bar that will perform on-change filtering on this column.
             If set to :click-to-filter, makes values searchable by clicking them"
    :filter-in-url "If false turns off filtering in url for the column, by default true"}])

(def control-map-help
  "The possible values of a control-map for a table, which should be a
  sequence of maps where each map corresponds to one column of the table."
  {:row-class-fn (fn [entry] "Provides the class (str or vector) of a :tr, given entry")
   :columns col-map-help
   :controls-left (fn [content] "fn of `content` to place before the paging controls (if any)")
   :controls-right (fn [content] "fn of `content` to place after the paging controls (if any)")
   :default-styling? "If truthy, apply default styles such as direction indicators on sorts"
   :table-id "Allows for each table to have a unique ID"
   :filter-in-url "If false turns off filtering in url for the table, by default true"
   :paging {:simple "If truthy, use a local atom and default setters and getters without bothering with anything else defined in `:paging`. "
            :get-current-screen (fn [] "Get the current screen num (0-based), from an atom or reframe, etc")
            :set-current-screen (fn [n] "Set current screen num. Default 0.")
            :get-final-screen (fn [] "Get the final screen num (0-based), from an atom or reframe, etc")
            :set-final-screen (fn [n] "Set final screen num.")
            :get-amount (fn [] "Get the number of entries visible per screen")
            :set-amount (fn [n] "Set number of entries visible per screen. Default 5.")
            :r-content [:div.icon "prev-page"]
            :rr-content [:div.icon "first page"]
            :f-content [:div.icon "next page"]
            :ff-content [:div.icon "final page"]
            :show-total-pages? "If truthy, it will show the current page and total pages in the middle content as opposed to just the current page."
            :left-bar-content [:div.whatever "Stuff before the controls"]
            :right-bar-content [:div.whatever "Stuff after the controls"]
            :entries-option {:values ["A vector of integers representing options for how many entries appear per page"]
                             :default "An integer showing the default value for how many entries appear per page"}
            :align "Choose either `left` or `right` or `center` for paging controls alignment. Default to `left`"}
   :table-scroll-bar {:fixed-columns {:first? "If truthy, the first column of the table will be fixed/sticky"
                                      :last? "If truthy, the last column of the table will be fixed/sticky"
                                      :table-color "Changes default values for the background colors to match different colored tables"}}
   :example/csv-download? "If truthy, a button will appear with the corresponding table for downloading the data in a csv file"
   :sort-filter-highlight "A string representing a CSS color to highlight columns that are currently being sorted or filtered"})

(defn table
  "Generate a table from `entries` according to headers and getter-fns in `controls`"
  [controls entries]
  (let [table-id (internal/generate-table-id (:table-id controls))]
    [:h1 "entry to table core"]
    #?(:clj (internal/_table controls entries table-id)
       :cljs (jscore/jstable controls entries table-id))))
