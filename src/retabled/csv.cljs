(ns retabled.csv)

(defn ^:private download-object-as-csv
  "Exports the value as a csv"
  [csv export-name]
  (let [data-blob (js/Blob. #js [csv] #js {:type "application/csv"})
        link (.createElement js/document "a")]
    (set! (.-href link) (.createObjectURL js/URL data-blob))
    (.setAttribute link "download" export-name)
    (.appendChild (.-body js/document) link)
    (.click link)
    (.removeChild (.-body js/document) link)))

(defn ^:private pop-str
  [item]
  (subs item 0 (- (count item) 1)))

(defn ^:private append-comma
  [item]
  (str item ","))

(defn ^:private csv-str
  [value]
  (let [str-val (str value)
        double-quotes (clojure.string/replace str-val "\"" "\"\"")
        front-and-back-quotes (str "\"" double-quotes "\"")]
    front-and-back-quotes))

(defn ^:private write-csv
  [col-of-maps]
  (let [schema (into [] (reduce #(into %1 (keys %2)) #{} col-of-maps))
        data (into [] (map (fn [item] (let [local-schema (into #{} (keys item))]
                                        (for [key schema]
                                          (let [value (key item)]
                                            (if (local-schema key) (csv-str value) "")))))) col-of-maps)
        schema-with-comma (into [] (map append-comma) schema)
        data-with-comma (into [] (map #(str (pop-str (apply str "" (into [] (map append-comma %1)))) "\n")) data)
        schema-str (pop-str (apply str schema-with-comma))
        data-str (apply str data-with-comma)]
    (str schema-str "\n" data-str)))

(defn download-items-as-csv
  "Turns `items` into a csv and downloads it as `export-name` with an optional `schema` as a collections of names for columns, otherwise the schema for the first item in `items` is used"
  [items export-name]
  (let [csv (write-csv items)]
    (download-object-as-csv csv export-name)))
