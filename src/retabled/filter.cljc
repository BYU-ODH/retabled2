(ns retabled.filter
  (:require [retabled.shared :as shared]
            #?(:cljs [reagent.core :refer [atom]])
            [clojure.string :as str]))

(def exceptions {:invalid-filterfn "Invalid filter-fn given to generate-filter-fn"
                 :resolve-filter "Unable to resolve filter"})

(def FILTER-MAP (atom {}))

(defn map-from-url []
  (let [search-string #?(:clj () :cljs (.getDecodedQuery (goog.Uri. (.-location js/window))))]
    (if-not
     (empty? search-string)
      #?(:clj ()
         :cljs
         (-> search-string
             js/URLSearchParams.
             js/Object.fromEntries
             (js->clj)
             (#(into {} (map (fn [[k v]] [k {:value v}])) %))))

      {})))

(def SEARCH-MAP (atom (map-from-url)))

(defn url-from-map []
  (let [remove-nils (remove (comp empty? :value second) @SEARCH-MAP)]
    (str (str/join "&" (map (fn [[k v]] (when (and k v)(str (name k) "=" (str/replace (:value v) "&" "%26")))) remove-nils)))))

(defn search-in-url
  []
  (let [current-uri #?(:clj () :cljs (goog.Uri. (.-location js/window)))
        complete-uri (.setQuery current-uri (url-from-map))]
    #?(:cljs (js/window.history.replaceState {}, "", (.toString complete-uri)))))

(defn sanitize-input-string
  "Takes an input string and escapes special characters that may affect regexp or URL mapping"
  [string]
  (let [esc-chars "()*&^%$#!?[]\\"]
    (str (->> string
              (replace (zipmap esc-chars (map #(str "\\" %) esc-chars)))
              (reduce str)))))

(defn generate-filter-fn
  "Produce the function which compares a filter-map to a map and deems it good or not. If a :key in `filter-map` doesn't exist when filtering `filterable-map`, the filterable map will fail this function."
  [filter-map]
  (fn [filterable-map]
    (every? some?
            (for [[k fm] filter-map
                  :let [f (:value fm)
                        i? (:ignore-case? fm)
                        filter-fn (:filter-fn fm)
                        field-filter-fn (cond
                                          (fn? f) f
                                          (string? f)
                                          (partial re-find (re-pattern (if i?
                                                                         (str "(?i)" (sanitize-input-string f))
                                                                         (sanitize-input-string f))))
                                          (int? f) #(= f %)
                                          :else (throw (ex-info (:invalid-filterfn exceptions) {:received {k f}})))]]
              ^{:key k}
              (when-let [filterable-value (filter-fn filterable-map)]
                (field-filter-fn (str filterable-value)))))))

(defn filter-by-map
  "Filter a collection of maps by a filter-map, where the filter map specifies the columns and the value to filter them by.
  Strings will be made into basic regexp."
  [filter-map map-coll]
  (let [filter-fn (generate-filter-fn filter-map)
        results (filter filter-fn map-coll)]
    results))

(defn on-change-filter
  "Returns a function that will be used for on-change events in the input filter bar"
  [{:keys [filter-in-url
           col-map
           filter-fn
           filter-address
           search-string
           search-map-address
           ignore-case?
           table-id]}]
  (if (or (false? filter-in-url) (false? (:filter-in-url col-map)))
    #(swap! FILTER-MAP assoc-in [table-id filter-address] {:value (shared/get-value-from-change %) :ignore-case? ignore-case? :filter-fn filter-fn})
    (do 
      (when-not (nil? search-string)
        (swap! FILTER-MAP assoc-in [table-id filter-address] {:value (:value search-string) :ignore-case? ignore-case? :filter-fn filter-fn}))
      #(swap! SEARCH-MAP assoc search-map-address {:value (shared/get-value-from-change %) :ignore-case? ignore-case? :filter-fn filter-fn}))))

(defn gen-filter
  "Generate an input meant to filter a column. `filter-address` is the key of this filter in `FILTER-MAP` and
  may be a function, keyword, etc, as specified by `(:valfn col-map)`"
  [{:as args
    :keys [col-map
           table-id
           filter-in-url]}]
  (let [ignore-case? (:ignore-case? col-map true)
        search-map-address (str table-id "-" (:headline col-map))
        id (shared/idify (:headline col-map))
        filter-fn (or (:filterfn col-map)(:valfn col-map))
        filter-address (str filter-fn)
        search-string (@SEARCH-MAP (str table-id "-" (:headline col-map)))]
    (when (contains? @SEARCH-MAP search-map-address)
      (swap! SEARCH-MAP assoc search-map-address (merge (@SEARCH-MAP search-map-address) {:ignore-case? ignore-case? :filter-fn filter-fn})))
    [:input.filter {:id (str id "_filter")
                    :value (or (:value search-string) (get-in @FILTER-MAP [table-id filter-address :value]))
                    :on-change (on-change-filter (conj args {:filter-fn filter-fn
                                                             :filter-address filter-address
                                                             :search-string search-string
                                                             :search-map-address search-map-address
                                                             :ignore-case? ignore-case?}))}]))

(defn filtering
  "Filter entries according to `FILTER-MAP`"
  [table-id entries]
  (cond->> entries
    (not-empty (@FILTER-MAP table-id)) (filter-by-map (@FILTER-MAP table-id))))

(defn on-click-filter
  "Changes the filter value based on value clicked"
  [{:as args
    :keys [col-map
           table-id
           filter-in-url
           value]}]
  (let [filter-fn (or (:filterfn col-map)(:valfn col-map))
        filter-address (str filter-fn)
        search-string (@SEARCH-MAP (str table-id "-" (:headline col-map)))
        val {:value value :filter-fn filter-fn}]
    (when-not (empty? value)
      (if (or (false? filter-in-url) (false? (:filter-in-url col-map)))
        #(swap! FILTER-MAP assoc-in [table-id filter-address] val)
        (do
          (when-not (nil? search-string)
            (swap! FILTER-MAP assoc-in [table-id filter-address] search-string))
          #(swap! SEARCH-MAP assoc (str table-id "-" (:headline col-map)) val))))))

(defn resolve-filter
  [controls entries]
  (when (and controls entries)
    (let [{:keys [valfn filterfn displayfn]} controls]
      (cond
        filterfn
        (filterfn entries)

        valfn
        (valfn entries)
        
        displayfn
        (displayfn entries)

        :else
        (throw (ex-info (:resolve-filter exceptions) entries))))))
