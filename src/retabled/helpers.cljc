(ns retabled.helpers "These are for the example"
  (:require [retabled.csv :as csv]))

(defn download-csv-button
  "Unspecified for clj"
  [items export-name]
  [:button {:on-click #(csv/download-items-as-csv items export-name)
            :style {"position" "sticky"
                    "right" "1em"
                    "backgroundColor" "white"}} "Download CSV"])

(defn render-csv-button-row
  "This function is only for examples and will not be included in production"
  [data table-id]
  (let [entries-to-download (get-in data [:current table-id])]
    [:tr.row.csv-button-row
     [:td.cell.csv-button {:colSpan 100 :style {"textAlign" "right"}}
      [download-csv-button entries-to-download (str table-id ".csv")]]]))
