(ns retabled.internal
  (:require [retabled.filter :as filter]
            [retabled.helpers :as helpers]
	    [retabled.sort :as sort]
            #?(:cljs [reagent.core :refer [atom]])
            [clojure.string :as str]
            [retabled.listeners :as listeners]))

(def exceptions {:duplicate-id "multiple tables with the same table-id"})

(def PAGING (atom {:default {:current-screen 0
                             :final-screen 0
                             :entries-per-page 10
                             :go-to-value "1"}}))

(def DATA (atom {:all {}
                 :current {}}))

(defn atom?
  "ducktype an atom as something dereferable"
  [a]
  (try (do (deref a) true)
       (catch #?(:clj Exception :cljs js/Error) _ false)))

(defn style-cell
  "creates logic for all of the cell styling"
  [{:as args
    :keys [arg-map
           c
           controls
           table-id]}]
  (let [table-color (get-in controls [:table-scroll-bar :table-color])
        fixed-column-style (fn [side] {:style {"position" "sticky"
                                               side "0"
                                               "backgroundColor" table-color}})
        first? (= c (first (:columns controls)))
        first-fixed? (get-in controls [:table-scroll-bar :first?])
        last? (= c (last (:columns controls)))
        last-fixed? (get-in controls [:table-scroll-bar :last?])
        sort-filter-highlight (:sort-filter-highlight controls)
        sorted? (or (and (get-in @sort/SORT-MAP [table-id :selected])
                         (= (:sortfn c) (get-in @sort/SORT-MAP [table-id :selected])))
                    (= (:valfn c) (get-in @sort/SORT-MAP [table-id :selected])))
        filtered? (> (count (get-in @filter/FILTER-MAP [table-id (str (:valfn c)) :value])) 0)
        style-map (if (and first? first-fixed?)
                    (merge arg-map (fixed-column-style "left"))
                    (if (and last? last-fixed?)
                      (merge arg-map (fixed-column-style "right"))
                      arg-map))
        background-style (if (and sort-filter-highlight (or sorted? filtered?))
                           sort-filter-highlight
                           table-color)]
    (assoc-in style-map [:style "backgroundColor"] background-style)))

(defn render-header-fields
  [{:as args
    :keys [controls
           table-id]}]
  (into [:tr.table-headers.row]
        (for [c (:columns controls)
              :let [h  (cond->> (:headline c)
                         (:sort c) (sort/gen-sort c table-id))
                    filter-input (when (:filter c) (filter/gen-filter {:col-map c
                                                                       :table-id  table-id
                                                                       :filter-in-url (:filter-in-url controls)}))
                    style-cell-args {:controls controls
                                     :table-id table-id
                                     :c c}
                    style-th (style-cell style-cell-args)]]
          ^{:key (:genkey c)}
          (if filter-input
            [:th style-th h [:br] filter-input]
            [:th style-th h]))))

(defn a-control-label
  [{:keys [content
           screen
           page?
           set-current-screen]}]
  (let [style-hidden {:style {"color" "transparent"}
                      :class "hidden"}]
    [:a.control-label (if page?
                        style-hidden
                        {:on-click #(set-current-screen screen)})
     content]))

(defn render-screen-controls
  "Render the controls to edit this screen for results"
  [{:as paging-controls
    :keys [get-current-screen
           get-amount
           set-amount
           set-current-screen
           set-final-screen
           get-final-screen
           r-content
           rr-content
           f-content
           ff-content
           left-bar-content
           right-bar-content
           num-columns
           entries-option
           align
           show-total-pages?]
    :or {num-columns 100}}
   table-scroll-bar
   table-id]
  (let [current-screen-for-display (inc (get-current-screen))
        prevfn #(max (dec (get-current-screen)) 0)
        nextfn #(min (inc (get-current-screen)) (get-final-screen))
        first-page? (= (get-current-screen) 0)
        last-page? (= (get-current-screen) (get-final-screen))
        style-page-to-go {:style {"width" "3em"
                                  "marginLeft" ".5em"}
                          :value (or (when (empty? (:go-to-value (@PAGING table-id)))
                                       "")
                                     current-screen-for-display)
                          :on-click #(swap! PAGING assoc-in [table-id :go-to-value] "")
                          :id "page-to-go"}
        on-change-page-to-go (fn [evt]
                               (let [val (-> evt .-target .-value)
                                     int-val (int val)]
                                 (when (= val "")
                                   (swap! PAGING assoc-in [table-id :go-to-value] val))
                                 (when (and (> int-val 0)(<= int-val (+ (get-final-screen) 1)))
                                   (do (swap! PAGING assoc-in [table-id :go-to-value] (str int-val))
                                       (set-current-screen (- int-val 1))))))
        table-color (:table-color table-scroll-bar)
        sc-map {:colSpan num-columns :style {"borderColor" table-color "textAlign" align}}
        div-conditional-styling (when table-scroll-bar {:style {"position" "sticky" align "1em"}})
        screen-num-content (if show-total-pages?
                             (str current-screen-for-display " of " (+ (get-final-screen) 1))
                             current-screen-for-display)
        page-to-go-map (assoc style-page-to-go :on-change on-change-page-to-go)
        entries-option-and-values-set (and entries-option (:values entries-option))
        entries-option-change (fn [evt]
                                (let [val (int (-> evt .-target .-value))]
                                  (swap! PAGING assoc-in [table-id :entries-per-page] val)
                                  (set-current-screen 0)
                                  (swap! PAGING assoc-in [table-id :go-to-value] "1")))]
    [:tr.row.screen-controls-row
     [:td.cell.screen-controls sc-map
      [:div.control div-conditional-styling
       left-bar-content
       [:div.control.first [a-control-label {:page? first-page?
                                             :screen 0
                                             :content rr-content
                                             :set-current-screen set-current-screen}]]
       [:div.control.prev [a-control-label {:page? first-page?
                                            :screen (prevfn)
                                            :content r-content
                                            :set-current-screen set-current-screen}]]
       [:div.control.current-screen [:span.screen-num screen-num-content]]
       [:div.control.next [a-control-label {:page? last-page?
                                            :screen (nextfn)
                                            :content f-content
                                            :set-current-screen set-current-screen}]]
       [:div.control.final [a-control-label {:page? last-page?
                                             :screen (get-final-screen)
                                             :content ff-content
                                             :set-current-screen set-current-screen}]]
      [:span.go-to "Go to"]
       [:input.page-to-go page-to-go-map]
       (when entries-option-and-values-set
        (let [values (distinct (:values entries-option))
              default (if (:default entries-option)
                        (:default entries-option)
                        (first values))]
          (into [:select.entries-per-page {:defaultValue default
                                           :onChange entries-option-change
                                           :style {"marginLeft" "1em"}}]
                (for [num values
                      :let [val-map {:value num}]]
                  ^{:key num}
                  [:option val-map  num]))))]
      right-bar-content]]))

(defn generate-theads
  "generate the table headers"
  [{:as args
    :keys [controls
           paging-controls
           table-id]}]
  (let [table-color (get-in controls [:table-scroll-bar :table-color])
        tsb-style {:style {"position" "sticky"
                       "top" "0"
                       "backgroundColor" table-color
                       "zIndex" "1"}}]
    [:thead (when (:table-scroll-bar controls)
              tsb-style)
     (when (:example/csv-download? controls)
     (helpers/render-csv-button-row @DATA table-id))
     (when (:paging controls)
       (render-screen-controls paging-controls (:table-scroll-bar controls) table-id))
     (render-header-fields {:controls controls
                            :table-id  table-id})]))

(defn generate-rows
  "Generate all the rows of the table from `entries`, according to `controls`"
  [{:as args
    :keys [controls
           entries
           table-id]}]
  (let [{:keys [row-class-fn columns sort-filter-highlight]
         :or {row-class-fn (constantly "row")}} controls]
    (into [:tbody]
          (for [e entries :let [tr ^{:key (:genkey e)} [:tr {:class (row-class-fn e)}]]]
            (into tr
                    (for [c columns :let [{:keys [valfn css-class-fn displayfn filter]                                         
                                           :or {css-class-fn (constantly "field")
                                                displayfn identity}} c
                                          arg-map (cond-> {:class (css-class-fn e)}
                                                    (= filter :click-to-filter) (assoc :on-click (filter/on-click-filter {:col-map c
                                                                                                                          :table-id  table-id
                                                                                                                          :filter-in-url (:filter-in-url controls)
                                                                                                                          :value  (filter/resolve-filter c e)}))
                                                    (= filter :click-to-filter) (assoc :class (str (css-class-fn e) " click-to-filter")))
                                          cell-style-args {:controls controls
                                                           :table-id table-id
                                                           :c c
                                                           :arg-map arg-map}
                                          cell-style (style-cell cell-style-args)]]
                      ^{:key (:genkey c)} [:td.cell cell-style
                                           (-> e valfn displayfn)]))))))

(defn default-paging
  "Set up a local atom and define paging functions with reference to it"
  [table-id]
  (let [paging-map {:get-current-screen #(if (< (:current-screen (@PAGING table-id)) (:final-screen (@PAGING table-id)))
                                           (:current-screen (@PAGING table-id))
                                           (do (when (not-empty (:go-to-value (@PAGING table-id)))
                                                 (swap! PAGING assoc-in [table-id :go-to-value] (str (+ 1 (:final-screen (@PAGING table-id))))))
                                               (:final-screen (@PAGING table-id))))
                    :set-current-screen #(do (swap! PAGING assoc-in [table-id :current-screen] %)
                                             (swap! PAGING assoc-in [table-id :go-to-value] (str (+ 1 %))))
                    :get-amount #(:entries-per-page (@PAGING table-id))
                    :set-amount #(swap! PAGING assoc-in [table-id :entries-per-page] %)
                    :get-final-screen #(:final-screen (@PAGING table-id))
                    :set-final-screen #(swap! PAGING assoc-in [table-id :final-screen] %)
                    :r-content "‹"
                    :rr-content "«"
                    :f-content "›"
                    :ff-content "»"
                    :align "left"}]
    paging-map))

(defn paging
  "Limit view of entries to a given screen.
  If `paging-controls` is falsy, do not filter."
  [paging-controls entries]
  (if-not paging-controls entries
          (let [{:keys [get-current-screen
                        get-amount
                        set-amount
                        set-current-screen
                        set-final-screen
                        get-final-screen]} paging-controls
                amt (get-amount)
                parted-entries (if (> amt (count entries))
                                 (list entries)
                                 (partition amt amt nil entries))
                max-screens (dec (count parted-entries))]                  
              (set-final-screen max-screens)
              (nth parted-entries (get-current-screen)))))

(defn curate-entries
  [{:as args
    :keys [paging-controls entries table-id]}]
  (when (not-empty entries)
    (->> entries
         (filter/filtering table-id)
         (sort/sorting table-id)
         (paging paging-controls))))

(def TABLE-NAME-COUNT (clojure.core/atom 0))

(defn check-for-duplicates
  [table-id]
  (when (get-in @DATA [:all table-id])
    (throw #?(:clj (Exception. (:duplicate-id exceptions)) :cljs (js/Error. (:duplicate-id exceptions))))))

(defn generate-table-id
  [table-id]
  (if-not table-id
    (do
      (swap! TABLE-NAME-COUNT inc)
      (str "__retabled-" @TABLE-NAME-COUNT))
    (do
      (try
        (check-for-duplicates table-id)
        (catch #?(:clj Exception :cljs js/Error) err
          #?(:clj (println err) :cljs (js/console.error err))))
      table-id)))

(defn update-data!
  [table-id entries k]
  (when table-id
    (swap! DATA assoc-in [k table-id] entries)))

(defn dissoc-data!
  [k]
  (swap! DATA dissoc k))

(defn update-default-entries-per-page!
  [controls table-id]
  (let [entries-option (get-in controls [:paging :entries-option])
        default (:default entries-option)
        values (:values entries-option)
        num (if default
              default
              (first values))]
    (when (and entries-option num)
      (swap! PAGING assoc-in [table-id :entries-per-page] num))))

(defn update-paging-info!
  [table-id]
  (swap! PAGING assoc table-id (:default @PAGING)))

(defn add-unique-key
  [items]
  (let [assoc-gensym (fn [item] (assoc item :genkey (gensym item)))]
    (map assoc-gensym items)))

(defn activate-listeners
  [controls table-id]
  (when (:paging controls)
    (listeners/activate PAGING table-id))
  (listeners/interval-function 1000 filter/search-in-url))

(defn update-atoms!
  [controls entries table-id]
  (when-not (@PAGING table-id)
    (update-paging-info! table-id)
    (update-default-entries-per-page! controls table-id))
  (update-data! table-id entries :all)
  (sort/initialize-sort-map table-id))

(defn render
  [table-id controls entries]
  (let [style-table {"height" "28em"
                         "width" "fit-content"
                         "maxWidth" "100%"
                         "display" "block"
                         "overflowY" "scroll"
                         "overflowX" "scroll"
                         "marginBottom" "3em"
                     "borderCollapse" "separate"}
        paging-controls (cond (get-in controls [:paging :simple])
                              (default-paging table-id)
                              
                              (:paging controls)
                              (merge (default-paging table-id)
                                     (:paging controls))
                              
                              :no-paging
                              nil)
        entries (curate-entries {:paging-controls paging-controls
                                 :entries entries
                                 :table-id table-id})]
    (update-data! table-id entries :current)
    [:table.table (assoc {:id table-id} :style (when (:table-scroll-bar controls)
                                                 style-table))
     [generate-theads {:controls controls
                       :paging-controls  paging-controls
                       :table-id  table-id}]
     [generate-rows {:controls controls
                     :entries  entries
                     :table-id table-id}]]))

(defn _table
  "Generate a table from `entries` according to headers and getter-fns in `controls`"
  [controls entries table-id]
  (activate-listeners controls table-id)
  (let [entries (add-unique-key entries)
        controls (assoc controls :columns (add-unique-key (:columns controls)))]
    (update-atoms! controls entries table-id)
    (fn interior-table [controls entries]
      [render table-id controls entries])))
