(ns retabled.jscore
  (:require [reagent.core :as reagent]
            [retabled.internal :as internal]))

(defn jstable
  [controls entries table-id]
  (reagent/create-class                 ;; <-- expects a map of functions
   {:display-name  "retabled-table"      ;; for more helpful warnings & errors

    :component-will-unmount #(internal/dissoc-data! :all)
    
    :reagent-render #(internal/_table controls entries table-id)}))
