(ns retabled.listeners)

(defn interval-function
  [interval function]
  (println "Interval function must be called from a .cljs scenario"))

(defn activate
  [PAGING-INFO]
  (println "This PAGING-INFO:" @PAGING-INFO "should have been sent to a .cljs file"))
