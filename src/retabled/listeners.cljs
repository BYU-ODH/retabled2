(ns retabled.listeners)

(defn interval-function
  [interval function]
  (js/window.setInterval function interval))

(defn on-document-click
  [e PAGING table-id]
  (when (and (some? e) (some? PAGING)))
  (let [page-to-go (js/document.getElementById "page-to-go")
        page-to-go-click? (when page-to-go
                            (.contains page-to-go e.target))]
    (when (not page-to-go-click?)
      (swap! PAGING assoc-in [table-id :go-to-value] (str (get-in @PAGING [table-id :current-screen]))))))

(defn activate
  [PAGING table-id]
  (js/document.addEventListener "click" #(on-document-click % PAGING table-id)))
