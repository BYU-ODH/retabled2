(ns retabled.sort
  (:require [clojure.string :as str]
            #?(:cljs [reagent.core :refer [atom]])))

(defn ascending
  []
  compare)

(defn descending
  []
  #(compare %2 %1))

(def default-sort
  "function to return a default sort map-atom"
  {:selected nil
   :direction ascending})

(def SORT-MAP (atom {}))

(defn initialize-sort-map
  "If a table-id is not yet found in `SORT-MAP`, then this function will associate the table-id provided with the `default-sort` map"
  [table-id]
  (when-not (get @SORT-MAP table-id)
    (swap! SORT-MAP assoc table-id default-sort)))

(defn sort-click
  "Select sort field; if sort field unchanged, sort direction"
  [valfn table-id]
  (let [currently-selected (get-in @SORT-MAP [table-id :selected])
        swap-dir #(if (= ascending  %) descending ascending)]
    (if (not= currently-selected valfn)
      (swap! SORT-MAP assoc-in [table-id :selected] valfn)
      (swap! SORT-MAP update-in [table-id :direction] swap-dir))))

(defn gen-sort
  "Render the title as a link that toggles sorting on this column"
  [c table-id headline]
  (let [sortfn (or (:sortfn c) (:valfn c))
        sorting-this? (= sortfn (get-in @SORT-MAP [table-id :selected]))
        sc (condp = (get-in @SORT-MAP [table-id :direction])
             ascending "ascending"
             descending "descending")
        classes (when sorting-this? ["sorting-by-this" sc])]
    [:a.sortable {:class classes :on-click #(sort-click sortfn table-id)} headline]))

(defn sorting
  "Sort given entries"
  [table-id entries]
  (if (and (get @SORT-MAP table-id) entries)
    (let [f (get-in @SORT-MAP [table-id :selected])
          dir (get-in @SORT-MAP [table-id :direction])]
      (if (and f dir)
        (sort-by f (dir) entries)
        entries))
    entries))
