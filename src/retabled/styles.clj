;; -*- eval: (rainbow-mode) -*-
(ns retabled.styles
  "Garden styles following the BYU schemes"
  (:require [garden.def :refer [defstylesheet defstyles defkeyframes]]
            [garden.units :as u]
            [garden.color :as c :refer [hex->hsl hsl->hex]] ;:rename {hex->rgb hr, rgb->hex rh}]
            [garden.selectors :as s :refer [nth-child]]))

;;;;;;;;;;;;
;; STYLES ;;
;;;;;;;;;;;;
(def sort-directions
  [:body
   [:.sorting-by-this.descending:after
    {:content "\"▲\""}]
   [:.sorting-by-this.ascending:after
    {:content "\"▼\""}]])

(defstyles main
  {:vendors ["webkit" "moz" "o" "ms"]}
  [:div.control {:display "inline-block"
                 :padding-right (u/em 1)}]
  [:table
   [:.cell {:max-width "100%"}]
   [:input.filter {:min-width "100%"
                   :width (u/em 2)}]
   [:td.click-to-filter {:cursor :pointer}
    [:&:hover {:text-decoration :underline}]]]
  [:a.hidden {:cursor "default"}]
  sort-directions)
