(ns retabled.core-front
  (:require [reagent.core :as r]
            [reagent.session :as session]
            [retabled.ajax :refer [load-interceptors!]]
            [retabled.shared-test :refer [page-template] :as shared]
            [retabled.routes :as routes]
            [retabled.core :as ret]
            [secretary.core :as secretary :include-macros true]
            [accountant.core :as accountant]
            [ajax.core :refer [GET POST]]))

(defn random-job
  "generate a random job"
  []
  (let [jobs ["programmer" "Dog catcher" "pizza deliverer" "pro-gamer" "President of the United States of America" "transition: all 0.2s ease-in-out 0s; font-size: 13px; border-top: 1px solid rgb(245, 245, 245); box-shadow: rgb(170, 170, 170) 0px 0px 1px; line-height: 18px; color: rgb(51, 51, 51); font-family: monospace; padding: 0px 10px 0px 70px; position: fixed; bottom: 0px; left: 0px; height: 0px; opacity: 0; box-sizing: border-box; z-index: 10000; text-align: left; border-radius: 0px; width: auto; background-color: transparent; min-height: 0px;"]]
    (rand-nth jobs)))

(defn gen-table-data
  "Generate `n` entries of random table data. Desc-map should have [key fun] pairs and fun will be evaluated each time to generate the data."
  [n desc-map]

  (into []
        (repeatedly n
                    #(into {}
                           (for [[k f] desc-map] [k (f)])))))

(def table-data
  (let [AMOUNT 50
        A (atom 0)]
    (r/atom (gen-table-data AMOUNT
                            {:name #(str "John Doe " (swap! A inc))
                             :job  random-job
                             :id   #(deref A)
                             :guid #(rand-int 1000)}))))

#_(swap! table-data into
         (gen-table-data 3
                         {:name #(str "Jane Doe " (rand-int 5))
                          :job  random-job
                          :id (constantly "nonsense")
                          :guid #(rand-int 1000)}))

(def table-data2
  (let [AMOUNT 50
        A (atom 0)]
    (r/atom (gen-table-data AMOUNT
                            {:name #(str "Jane Dear " (swap! A inc))
                             :job  random-job
                             :id   #(deref A)
                             :guid #(rand-int 1000)}))))


(defn empty-link
  "Make an empty link thing for each entry with the text inside"
  [val]
  [:a {:href     val
       :on-click #(js/alert (str "You clicked on " val))} val])

(defn my-valfn
  "name or job"
  [entry]
  (:name entry (:job entry)))

(def TABLE (r/atom 1))

(defn home-page []
  ;; Because the second table columns has its `:valfn` as `my-valfn`, 
  ;; `retabled.core-front/myvalfn` is the address for the filter. 
  ;; The filter can programaticaly be changed like so:
  ;; (swap! retabled.core/FILTER-MAP assoc retabled.core-front/my-valfn "2")

  (let [controls {:paging {:rr-content        "First"
                           :entries-option {:values [5 10 15 25 50 100]
                                            :default 15}
                           :show-total-pages? true}
                  :columns [{:valfn   :id
                             :headline  "ID"
                             :sortfn    (fn [entry] (let [id (:id entry)]
                                                      (cond
                                                        (> 3 id)  (- 5 id)
                                                        (<= 2 id) id)))
                             :sort      true
                             :filter    true}
                            {:valfn   my-valfn
                             :displayfn empty-link
                             :headline  "Name"
                             :sort      true
                             :filter    true}
                            {:valfn    :job
                             :sort     true
                             :filter   :click-to-filter
                             :headline "Job"}
                            {:valfn    :guid
                             :sort     true
                             :sortfn   #(:guid %)
                             :headline "GUID"}]
                  :table-scroll-bar {:first? true
                                     :last? true
                                     :table-color "white"}
                  :example/csv-download? true
                  :sort-filter-highlight "#ffcccb"
                  :table-id :first}]
    [:div.content
     [:section.hero
      [:div.hero-body
       [:h1.title "Retabled"]]]
     #_[:div.dat "My data:"
        (prn-str @table-data)]
     [:button {:on-click #(if (= @TABLE 1)
                            (swap! TABLE (constantly 2))
                            (swap! TABLE (constantly 1)))}
      (str "Switch from Table " @TABLE)]
     [:div.table1
        [:h2.title "Table 1"]
      [ret/table controls @table-data]]]))

(defn home-page2 []
  ;; Because the second table columns has its `:valfn` as `my-valfn`, 
  ;; `retabled.core-front/myvalfn` is the address for the filter. 
  ;; The filter can programaticaly be changed like so:
  ;; (swap! retabled.core/FILTER-MAP assoc retabled.core-front/my-valfn "2")

  (let [controls2 {:paging  {:rr-content        "First"}
                   :columns [{:valfn     :id
                              :headline  "ID"
                              :sortfn    (fn [entry] (let [id (:id entry)]
                                                       (cond
                                                         (> 3 id)  (- 5 id)
                                                         (<= 2 id) id)))
                              :sort      true
                              :filter    true}
                             {:valfn     my-valfn
                              :displayfn empty-link
                              :headline  "Name"
                              :sort      true
                              :filter    true}
                             {:valfn    :job
                              :sort     true
                              :filter true
                              :headline "Job"}
                             {:valfn    :guid
                              :sort     true
                              :sortfn   #(:guid %)
                              :headline "GUID"}]
                   :table-id :second}]
    [:div.content
     [:section.hero
      [:div.hero-body
       [:h1.title "Retabled"]]]
     #_[:div.dat "My data:"
        (prn-str @table-data)]
     [:button {:on-click #(if (= @TABLE 1)
                            (swap! TABLE (constantly 2))
                            (swap! TABLE (constantly 1)))}
      (str "Switch from Table " @TABLE)]
     [:div.table2
        [:h2.title "Table 2"]
      [ret/table controls2 @table-data2]]]))

(def pages {:home #'home-page})

(def pages2 {:home #'home-page2})

(defn page []
  (if (= @TABLE 1)
    [(pages (session/get :page))]
    [(pages2 (session/get :page))]))


;; -------------------------
;; Initialize app

(defn mount-components []
  (r/render [#'page] (.getElementById js/document "app")))

(defn init! []
  (load-interceptors!)
  (accountant/dispatch-current!)
  (mount-components))
