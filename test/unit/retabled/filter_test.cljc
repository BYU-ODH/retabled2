(ns retabled.filter-test
  (:require [clojure.test :refer [deftest is testing]]
            [retabled.filter :as filter]))

(deftest resolve-filter "Tests that this function will identify the right filterfn, ignore outside cases, and throw an exception when necessary"
  (let [resolve-filter-test (fn [expected column entry & assertion-string]
                              (is (= expected (filter/resolve-filter column entry)) (first assertion-string)))
        resolve-filter-test-error (fn [column entry & assertion-string]
                                    (is (thrown-with-msg? Exception (re-pattern (:resolve-filter filter/exceptions)) (filter/resolve-filter column entry)) (first assertion-string)))]

    (testing "Correctly handles nil controls and/or entries"
        (resolve-filter-test nil nil nil "Testing all nil values")
        (resolve-filter-test nil nil {:id 1, :name "tom"} "Testing all nil values except for entry")
        (resolve-filter-test nil {:valfn :name} nil "Testing all nil values except for column"))
    (testing "Correctly handles nil valfn and/or displayfn"
      (resolve-filter-test-error {} {:id 1, :name "tom"} "Testing nil valfn and displayfn")
      (resolve-filter-test "tom" {:displayfn :name} {:id 1, :name "tom"} "Testing nil valfn")
      (resolve-filter-test "tom" {:valfn :name} {:id 1, :name "tom"} "Testing nil displayfn"))
    (testing "Correctly handles nil valfn in entry"
      (resolve-filter-test "" {:valfn :name} {:id 1, :last-name "trump"} "Testing valfn not present in entry"))
    (testing "Correctly handles nil displayfn in entry"
        (resolve-filter-test "" {:displayfn :name} {:id 1, :last-name "trump"} "Testing displayfn not present in entry"))
    (testing "Correctly handles non-string entry values"
      (resolve-filter-test-error {:valfn :id} {:id 1} "Testing numerical entry value (valfn)")
      (resolve-filter-test-error {:displayfn :id} {:id 1} "Testing numerical entry value (displayfn)"))))

(deftest str-to-pattern "Tests that values provided will be converted to the correct string"
  (let [str-to-pattern-test (fn [expected string & assertion-string]
                              (is (= expected (filter/sanitize-input-string string)) (first assertion-string)))]
    (testing "Correctly handles nil string"
      (str-to-pattern-test "" nil "Test conversion from nil to empty string"))
    (testing "Correctly handles ordinary string"
      (str-to-pattern-test "ordinary string" "ordinary string"))
    (testing "Correctly handles all escaped chars"
      (str-to-pattern-test "\\(\\)\\*\\&\\^\\%\\$\\#\\!\\?\\[\\]\\\\" "()*&^%$#!?[]\\" "Escapses all escaped characters"))))

(deftest url-from-map "Tests that a correct URL extension will be made from the updated SEARCH-MAP atom"
  (let [url-from-map-test (fn [expected & assertion-string]
                            (is (= expected (filter/url-from-map)) (first assertion-string)))
        assoc-search-map! (fn [k m]
                           (swap! filter/SEARCH-MAP assoc k m))
        empty-search-map! (fn []
                           (swap! filter/SEARCH-MAP empty))]
    (testing "Correctly handles empty search map"
      (empty-search-map!)
      (url-from-map-test "" "Empty SEARCH-MAP should return an empty string"))
    (testing "Correctly handles nil or empty values"
      (empty-search-map!)
      (assoc-search-map! nil {:value "value"})
      (url-from-map-test "" "Nil key in SEARCH-MAP should return an empty string")
      (empty-search-map!)
      (assoc-search-map! :name {:value nil})
      (url-from-map-test "" "Nil value in SEARCH-MAP should return an empty string")
      (assoc-search-map! :name {:value ""})
      (url-from-map-test "" "Empty string in value should behave like a nil"))
    (testing "Correctly handles single value"
      (empty-search-map!)
      (assoc-search-map! :name {:value "value"})
      (url-from-map-test "name=value" "Single value should be added to URL"))
    (testing "Correctly handles single value with special character"
      (empty-search-map!)
      (assoc-search-map! :name {:value "va&lue"})
      (url-from-map-test "name=va%26lue" "Single value with special character should be manipulated and added to URL"))))
