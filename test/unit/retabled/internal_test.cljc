(ns retabled.internal-test
  (:require [clojure.test :refer [deftest is testing]]
            [retabled.internal :as internal]
            [retabled.sort :as sort]))

(deftest update-default-entries-per-page! "Tests that a reasonable default entries per page will be provided despite what the function is given"
  (let [entries-test (fn [expected controls & assertion-string]
                       (internal/update-default-entries-per-page! controls)
                       (let [actual (:entries-per-page @internal/PAGING-INFO)]
                         (is (= expected actual) (first assertion-string))))]
    (testing "Correctly updates when paging is nil"
      (entries-test (:entries-per-page @internal/PAGING-INFO) {} "Default shouldn't update when paging is nil"))
    (testing "Correctly updates when entries-option is nil"
      (entries-test (:entries-per-page @internal/PAGING-INFO) {:paging true} "Default shouldn't update when entries-option is nil"))
    (testing "Correctly updates when default and values are nil"
      (entries-test (:entries-per-page @internal/PAGING-INFO) {:paging {:entries-option true}} "Default shouldn't update when entries-option's default and values are nil"))
    (testing "Correctly updates when default is set and values are nil"
      (entries-test 20 {:paging {:entries-option {:default 20}}} "Default value provided should be the new default"))
    (testing "Correctly updates when default is nil and values are set"
      (entries-test 3 {:paging {:entries-option {:values [3 20 100]}}} "First number in values should be the new default"))
    (testing "Correctly updates when default is set and values are set"
      (entries-test 20 {:paging {:entries-option {:default 20 :values [3 20 100]}}} "Default value provided takes precedence over first number in values"))
    (testing "Correctly updates when default is nil and first value is nil"
      (entries-test (:entries-per-page @internal/PAGING-INFO) {:paging {:entries-option {:values [nil]}}} "Default shouldn't update when entries-option's default is nil and the first of the values is nil"))))

(deftest update-current-data! "Tests that only reasonable requests will update the CURRENT-DATA atom"
  (let [current-data-test (fn [expected table-id entries & assertion-string]
                            (internal/update-current-data! table-id entries)
                            (let [actual @internal/CURRENT-DATA]
                              (is (= expected actual) (first assertion-string))))]
    (testing "Correctly updates when table-id or entries are nil"
      (current-data-test @internal/CURRENT-DATA nil nil)
      (current-data-test (merge {"1" nil} @internal/CURRENT-DATA) "1" nil)
      (current-data-test @internal/CURRENT-DATA nil [{:first "entry"}{:second "entry"}]))
    (testing "Correctly updates when table-id and entries are set"
      (current-data-test (merge {"2" [{:first "entry"}{:second "entry"}]} @internal/CURRENT-DATA) "2" [{:first "entry"}{:second "entry"}]))))

(deftest update-all-data! "Tests that only reasonable requests will update the ALL-DATA atom"
  (let [all-data-test (fn [expected table-id entries & assertion-string]
                            (internal/update-all-data! table-id entries)
                            (let [actual @internal/ALL-DATA]
                              (is (= expected actual) (first assertion-string))))]
    (testing "Correctly updates when table-id or entries are nil"
      (all-data-test @internal/ALL-DATA nil nil)
      (all-data-test (merge {"1" nil} @internal/ALL-DATA) "1" nil)
      (all-data-test @internal/ALL-DATA nil [{:first "entry"}{:second "entry"}]))
    (testing "Correctly updates when table-id and entries are set"
      (all-data-test (merge {"2" [{:first "entry"}{:second "entry"}]} @internal/ALL-DATA) "2" [{:first "entry"}{:second "entry"}]))))

(deftest generate-table-id "Tests that table-id's will be approved when duplicates aren't present or generated when duplicates are present or no id is provided"
  (let [table-id-test (fn [expected table-id & assertion-string]
                        (is (= expected (internal/generate-table-id table-id)) assertion-string))
        generate-id (fn [] (str "__retabled-" (+ 1 @internal/TABLE-NAME-COUNT)))]
    (testing "Correctly generates new table-id"
      (table-id-test (generate-id) nil "Returns unique id"))
    (testing "Correctly assigns provided table-id"
      (let [id (generate-id)]
        (table-id-test id id "Accepts id given")))
    (testing "Correctly assigns data structure as table-id"
      (let [id (generate-id)]
        (table-id-test ["vector" "of" "strings" id] ["vector" "of" "strings" id] "Accepts data structure given as id")))))

(deftest check-for-duplicates "Tests that function identifies and warns against duplicate table-id's"
  (testing "Throws error for duplicate table-id's"
    (internal/update-all-data! :id [{}])
    (is (thrown-with-msg? Exception (re-pattern (:duplicate-id internal/exceptions)) (internal/check-for-duplicates :id))))
  (testing "Handles nil table-id value"
    (is (= nil (internal/check-for-duplicates nil)))))

(deftest curate-entries "Tests that entries are curated properly between sorting, filtering, and paging"
  (let [entries-test (fn [expected args & assertion-string]
                       (is (= expected (internal/curate-entries args)) (first assertion-string)))
        entries [{:id 1 :name "tom"}
                 {:id 2 :name "jerry"}
                 {:id 3 :name "mario"}
                 {:id 4 :name "luigi"}]]
    (testing "Correctly handles nil and empty entries"
      (entries-test nil {:paging-controls nil
                         :entries nil
                         :SORT (atom {})
                         :FILTER (atom {})} "Returns nil when entries are nil")
      (entries-test nil {:paging-controls nil
                         :entries []
                         :SORT (atom {})
                         :FILTER (atom {})} "Returns nil when entries are empty"))
    (testing "Correctly pages"
      (entries-test '({:id 1, :name "tom"}
                      {:id 2, :name "jerry"})
                    {:paging-controls (merge (internal/default-paging) {:get-amount (constantly 2)})
                     :entries entries
                     :SORT (atom {})
                     :FILTER (atom {})} "Returns newly paged entries"))
    (testing "Correctly filters characters"
        (entries-test '({:id 2 :name "jerry"}
                        {:id 3 :name "mario"})
                      {:paging-controls nil
                       :entries entries
                       :SORT (atom {})
                       :FILTER (atom {:name {:value "r"}})} "Returns filtered entries by a single character"))
    (testing "Correctly filters strings"
      (entries-test '({:id 3, :name "mario"})
                    {:paging-controls nil
                     :entries entries
                     :SORT (atom {})
                     :FILTER (atom {:name {:value "io"}})} "Returns filtered entries by a string"))
    (testing "Correctly filters numbers"
        (entries-test '({:id 2, :name "jerry"})
                      {:paging-controls nil
                       :entries entries
                       :SORT (atom {})
                       :FILTER (atom {:id {:value "2"}})} "Returns filtered entries by a number"))
    (testing "Correctly sorts numbers"
      (entries-test '({:id 4, :name "luigi"}
                      {:id 3, :name "mario"}
                      {:id 2, :name "jerry"}
                      {:id 1, :name "tom"})
                    {:paging-controls nil
                     :entries entries
                     :SORT (atom {:selected :id :direction sort/descending})
                     :FILTER (atom {})} "Returns entries sorted by a number"))
    (testing "Correctly sorts strings"
        (entries-test '({:id 2, :name "jerry"}
                        {:id 4, :name "luigi"}
                        {:id 3, :name "mario"}
                        {:id 1, :name "tom"})
                      {:paging-controls nil
                       :entries entries
                       :SORT (atom {:selected :name :direction sort/ascending})
                       :FILTER (atom {})} "Returns entries sorted by a string"))
    (testing "Correctly pages, filters, and sorts together"
      (entries-test '({:id 1, :name "tom"})
                    {:paging-controls (merge (internal/default-paging) {:get-amount (constantly 2)})
                     :entries entries
                     :SORT (atom {:selected :id :direction sort/descending})
                     :FILTER (atom {:name {:value "om"}})} "Returns entries after sorting, filtering, and paging are complete"))))
