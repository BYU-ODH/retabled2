(ns retabled.sort-test
  (:require [clojure.test :refer [deftest is testing]]
            [retabled.sort :as sort]))

(deftest sorting "Testing that this function will sort correctly and properly run through outside cases."
  (let [sorting-test (fn [expected SORT entries & assertion-string]
                       (is (= expected (sort/sorting SORT entries)) (first assertion-string)))
        entries [{:name "Greg" :id 1}{:name "George" :id 2}{:name "Carter" :id 3}{:name "Leif" :id 0}]]
    (testing "Correctly handles nil SORT and/or entries"
        (sorting-test nil nil nil "All values nil")
        (sorting-test [{:name "name"}] nil [{:name "name"}] "All values nil except for entries")
        (sorting-test nil (atom {:selected :name :direction sort/ascending}) nil "All values nil except for SORT atom"))
    (testing "Correctly handles nil :sorted and/or :direction"
      (sorting-test entries (atom {:selected nil :direction nil}) entries "Tests nil selected and direction values")
      (sorting-test entries (atom {:selected :name :direction nil}) entries "Tests nil direction value")
      (sorting-test entries (atom {:selected nil :direction sort/ascending}) entries "Tests nil selected value"))
    (testing "Correctly sorts single set of entries"
      (sorting-test `({:name "Leif" :id 0}{:name "Greg" :id 1}{:name "George" :id 2}{:name "Carter" :id 3}) (atom {:selected :id :direction sort/ascending}) entries "Sorts set of entries by number")
      (sorting-test `({:name "Carter" :id 3}{:name "George" :id 2}{:name "Greg" :id 1}{:name "Leif" :id 0}) (atom {:selected :name :direction sort/ascending}) entries "Sorts set of entries by string"))))

(deftest sort-click "Testing that the SORT atom will update correctly, even with outside cases."
  (let [sort-click-test (fn [expected-selected expected-direction valfn SORT
                             & assertion-strings]
                          (sort/sort-click valfn SORT)
                          (is (= expected-selected (:selected @SORT)) (first assertion-strings))
                          (is (= expected-direction (:direction @SORT)) (second assertion-strings)))]
    (testing "Correctly handles nil values"
      (sort-click-test nil sort/ascending nil (atom {:selected nil :direction nil}) "Tests nil valfn, selected, and direction")
      (sort-click-test :name nil :name (atom {:selected nil :direction nil}) "Tests nil selected and direction with a given valfn"))
    (testing "Correctly switches direction for already selected valfn"
      (sort-click-test :name sort/descending :name (atom {:selected :name :direction sort/ascending})))
    (testing "Correctly switches selected for different valfn"
      (sort-click-test :id sort/ascending :id (atom {:selected :name :direction sort/ascending})))))
